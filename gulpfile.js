// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();


/*// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('pre/js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});*/

//browserSync
gulp.task('sass', function() {
  return gulp.src('pre/scss/**/*.scss') // Gets all files ending with .scss in app/scss
    .pipe(sass())
    .pipe(gulp.dest('pre/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('browserSync', function() {
      browserSync.init({
          port: 9000,
          server: {
            baseDir: 'pre/'
          }
    });
});
gulp.task('uglify', function() {
  return gulp.src('pre/js/**/*.js') // Gets all files ending with .scss in app/scss
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('pre/scss/**/*.scss', ['sass']);
  // Reloads the browser whenever HTML or JS files change
  gulp.watch('pre/*.html', browserSync.reload);
  gulp.watch('pre/js/**/*.js', browserSync.reload);
});


// Default Task
gulp.task('default', ['sass', 'uglify', 'watch', 'browserSync']);
