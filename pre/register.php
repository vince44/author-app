<?php
require_once 'core/init.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <title>Author App</title>
<!-- build:css public/css/style.css -->
	<link rel="stylesheet" href="css/main.css">
<!-- endbuild -->
</head>

<body>
	<div class="container-fluid" id="firstNav">
		<p id="greeting">Welcome to first modern Author Application</p>
	</div>

	<nav class="navbar navbar-custom">
		<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="glyphicon glyphicon-menu-hamburger"></span>
					</button>
					<a class="navbar-brand" href="login.html">AuthorApp</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="index.php">Login</a></li>
						<li><a href="register.php">Register</a></li>
						</ul>
			 </div>
			 </div>
	</nav>

	<div class="page-header" id="thirdNav">
		<h2 id="h2">Author app version 1.0</h2>
		<p id="info">This app is demo version and it`s build only as school project.</br>
			If you like this app or having a problem you can contact us on this email: </br>uroscosic4@gmail.com</br>
		</p>
		<p id="copyright">@Copyright Uros Cosic 2017</p>
	</div>

			<!-- Tab Register-->
	    <div role="tabpanel" class="col-lg-6" id="register">
				<h3 id="h3_register">Register</h3>
				<div class="col-lg-6 register-group">
					<?php

						if(input::exists()){
							if(token::check(input::get('token'))){
								$validate = new validate();
								$validations = $validate->check($_POST, array(
									'firstName' => array(
										'required' => true),
									'lastName' => array(
										'required' => true),
									'username' => array(
										'required' => true,
										'min' => 2,
										'max' => 40,
										'unique' => 'users'),
									'password' => array(
										'required' => true,
										'min' => 8),
									're_pass' => array(
										'required' => true,
										'matches' => 'password'),
									'email' => array(
										'required' => true),
									'employee_num' => array(
										'required' => true,
										'min' => 4)
								));
								if($validations->passed()){
									$user = new user();
									try{
										$user->create(array(
											'firstName' => input::get('firstName'),
											'lastName' => input::get('lastName'),
											'username' => input::get('username'),
											'password' => Hash::make(input::get('password')),
											'email' => input::get('email'),
											'employee_num' => input::get('employee_num'),
											'grupa' => 1
										));

									}catch(Exception $e){
										die($e->getMessage());
									}
								}else{
									foreach ($validations->errors() as $error) {
										echo $error, '</br>';
									}
								}
							}
						}
					?>

					<form action="" method="post">
						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3">@</span>
							<input type="text" class="form-control" id="first_name" name="firstName" placeholder="First Name" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3">@</span>
							<input type="text" class="form-control" id="last_name" name="lastName" placeholder="Last Name" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3">@</span>
							<input type="text" class="form-control" id="new_username" name="username" placeholder="New Username" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3"><i class="fa fa-lock" aria-hidden="true"></i></span>
							<input type="password" class="form-control" id="new_password" name="password" placeholder="New Password" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3"><i class="fa fa-lock" aria-hidden="true"></i></span>
							<input type="password" class="form-control" id="re_password" name="re_pass" placeholder="Again Password" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="new_password" name="email" placeholder="Email address" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<div class="input-group input-group-md">
							<span class="input-group-addon" id="sizing-addon3"><i class="fa fa-id-card-o" aria-hidden="true"></i></span>
							<input type="text" class="form-control" id="new_password" name="employee_num" placeholder="Employee Number" aria-describedby="sizing-addon3" autocomplete="off" />
						</div>

						<input type="hidden" name="token" value="<?php echo token::generate(); ?>" />
						<button class="btn btn-success" type="submit" id="btn_register">Register</button>
					</form>
				</div>
			</div>

<!-- Latest compiled and minified JavaScript -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/main.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
