<?php
  class user{
    private $_db,
            $_data,
            $_sessionName,
            $_isLoggedIn;

    public function __construct($user = null){
        $this->_db = db::getInstance();

        $this->_sessionName = config::get('session/session_name');

        if(!$user){
          if(session::exists($this->_sessionName)){
            $user = session::get($this->_sessionName);

            if($this->find($user)){
              $this->_isLoggedIn = true;
            }
          }
        }else{
          $this->find($user);
        }
    }

    public function create($fields = array()){
      if(!$this->_db->insert('users', $fields)){
        throw new Exception('There was a problem creating an account');
      }
    }

    public function find($user = null){
      if($user){
        $field = (is_numeric($user)) ? 'user_id' : 'username';
        $data = $this->_db->get('users', array($field, '=', $user));

        if($data->count()){
          $this->_data = $data->first();
          return true;
        }
      }
      return false;
    }

    public function login($username = null, $password = null){
      $user = $this->find($username);

      if($user){
        if($this->data()->password === Hash::make($password, $this->data())){
          session::put($this->_sessionName, $this->data()->user_id);
          return true;
        }
      }

      return false;
    }

    public function logout(){
      session::delete($this->_sessionName);
    }

    public function data(){
      return $this->_data;
    }

    public function isLoggedIn(){
      return $this->_isLoggedIn;
    }

  }
