$(function(){

  //Dela sakrivanje i pokazivanje novih polja za upisivanje dela
  $('#dela').hide();

  $('#novoDelo').click(function(){
    $('#dela').show();

    $('#izmeni_delo').hide();

    $('#obrisi_delo').hide();


    $('#delo_table').hide();

    $('#pretrazi_delo').hide();

  });

  /////////////////////////////////////////////////////

  $('#izmeni_delo').hide();

  $('#izmeniDelo').click(function(){
    $('#izmeni_delo').show();

    $('#dela').hide();

    $('#obrisi_delo').hide();

    $('#delo_table').hide();

    $('#pretrazi_delo').hide();
  });

  ////////////////////////////////////////////////////
  $('#obrisi_delo').hide();

  $('#obrisi_d').click(function(){
    $('#obrisi_delo').show();

    $('#izmeni_delo').hide();

    $('#dela').hide();

    $('#delo_table').hide();

    $('#pretrazi_delo').hide();
  });
/////////////////////////////////////
  $('#delo_table').hide();

  $('#lista_dela').click(function(){
    $('#delo_table').show();

    $('#dela').hide();

    $('#izmeni_delo').hide();

    $('#obrisi_delo').hide();

    $('#pretrazi_delo').hide();


  });

  //////////////////////////////////////

  $('#pretrazi_delo').hide();

  $('#pretrazi').click(function(){
      $('#pretrazi_delo').show();

      $('#dela').hide();

      $('#izmeni_delo').hide();

      $('#obrisi_delo').hide();

      $('#delo_table').hide();


  });


});
