$(function(){

  $("#day_error_message").hide();
  $("#month_error_message").hide();
  $("#year_error_message").hide();

  var error_day = false;
  var error_month = false;
  var error_year = false;

  $("#form_day").focusout(function(){
    check_day();
  });

  $("#form_month").focusout(function(){
    check_month();
  });

  $("#form_year").focusout(function(){
    check_year();
  });



  function check_day() {
    var day_value = $("#form_day").val();
    var day_length = $("#form_day").val().length;

    if(day_value > 30 || day_length < 2 || isNaN(day_value) === true) {
      $("#day_error_message").html("Invalid input");
      $("#form_day").css("border", "1px solid red");
      $("#day_error_message").show();
      error_day = true;
    }else {
      $("#day_error_message").hide();
      $("#form_day").css("border", "2px solid green");
    }
  }



  function check_month() {
    var month_value = $("#form_month").val();
    var month_length = $("#form_month").val().length;

    if(month_value > 12 || month_length < 2 || isNaN(month_value) === true) {
      $("#month_error_message").html("Invalid input");
      $("#form_month").css("border", "1px solid red");
      $("#month_error_message").show();
      error_month = true;
    }else {
      $("#month_error_message").hide();
      $("#form_month").css("border", "2px solid green");
    }
  }


  function check_year() {
    var year_value = $("#form_year").val();
    var year_length = $("#form_year").val().length;

    if(year_value > 2999 || year_value < 1930 || year_length < 4 || isNaN(year_value) === true) {
      $("#year_error_message").html("Invalid input");
      $("#form_year").css("border", "1px solid red");
      $("#year_error_message").show();
      error_month = true;
    }else {
      $("#year_error_message").hide();
      $("#form_year").css("border", "2px solid green");
    }
  }

// check form To

  $("#day2_error_message").hide();
  $("#month2_error_message").hide();
  $("#year2_error_message").hide();

  var error_day2 = false;
  var error_month2 = false;
  var error_year2 = false;

  $("#form_day2").focusout(function(){
    check_day2();
  });

  $("#form_month2").focusout(function(){
    check_month2();
  });

  $("#form_year2").focusout(function(){
    check_year2();
  });



  function check_day2() {
    var day2_value = $("#form_day2").val();
    var day2_length = $("#form_day2").val().length;

    if(day2_value > 30 || day2_length < 2 || isNaN(day2_value) === true) {
      $("#day2_error_message").html("Invalid input");
      $("#form_day2").css("border", "1px solid red");
      $("#day2_error_message").show();
      error_day2 = true;
    }else {
      $("#day2_error_message").hide();
      $("#form_day2").css("border", "2px solid green");
    }
  }



  function check_month2() {
    var month2_value = $("#form_month2").val();
    var month2_length = $("#form_month2").val().length;

    if(month2_value > 12 || month2_length < 2 || isNaN(month2_value) === true) {
      $("#month2_error_message").html("Invalid input");
      $("#form_month2").css("border", "1px solid red");
      $("#month2_error_message").show();
      error_month2 = true;
    }else {
      $("#month2_error_message").hide();
      $("#form_month2").css("border", "2px solid green");
    }
  }


  function check_year2() {
    var year2_value = $("#form_year2").val();
    var year2_length = $("#form_year2").val().length;

    if(year2_value > 2999 || year2_value < 1930 || year2_length < 4 || isNaN(year2_value) === true) {
      $("#year2_error_message").html("Invalid input");
      $("#form_year2").css("border", "1px solid red");
      $("#year2_error_message").show();
      error_month2 = true;
    }else {
      $("#year2_error_message").hide();
      $("#form_year2").css("border", "2px solid green");
    }
  }

  $("#form_year2").blur(function() {
    var year1 = $("#form_year").val();
    var year2 = $("#form_year2").val();
    if(year2 < year1) {
        alert('Invalid year');
        $("#form_year2").css("border", "1px solid red");
    }
  });

  //WEATHER SHOW
  $("#openweathermap-widget").load('app.php');

  function ajaxCall() {
  $.ajax({
      url: "app.php",
      success: (function (data) {
          $("#openweathermap-widget").html(data);
      })
  })
};
setInterval(ajaxCall, (10 * 1000)); // x * 1000 to get it in seconds

});
