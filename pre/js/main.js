$(function(){

  /*===============================================
            Register inputs validation
  ===============================================*/

  $("#new_username").focusout(function(){
    check_newUser();
  });

  $("#new_password").focusout(function(){
    check_newPassword();
  });

  $("#re_password").focusout(function(){
    check_newPassword();
  });

  function check_newUser(){
    var newUser = $("#new_username").val();

    if(newUser.length <= 3){
      $("#new_username").css("border", "2px solid red");
    }else{
      $("#new_username").css("border", "2px solid green");
    }
  };

  function check_newPassword(){
    var newPass = $("#new_password").val();
    var rePass = $("#re_password").val();

    if(newPass == "" || newPass.length < 8){
      $("#new_password").css("border", "2px solid red");
    }else{
      $("#new_password").css("border", "2px solid green");
    }

    if(newPass == rePass){
      $("#re_password").css("border", "2px solid green");
    }else{
      $("#re_password").css("border", "2px solid red");
    }
  };


});
