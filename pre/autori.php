<?php
	require_once 'core/init.php';
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--Bower css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <title>Author App</title>
<!-- build:css public/css/style.css -->
	<link rel="stylesheet" href="css/autori.css">
<!-- endbuild -->
</head>

<body>
	<nav class="navbar navbar-custom">
		<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="glyphicon glyphicon-menu-hamburger"></span>
					</button>
					<a class="navbar-brand" href="profile.php">AuthorApp</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li><a href="profile.php">Dela</a></li>
						<li><a href="search.php">Pretraga dela</a></li>
						<li><a href="autori.php">Autori</a></li>
						<li><a href="databse.php">Baza</a></li>
						<li><a href="account.php">Obracun</a></li>
						</ul>
						<?php
							$user = new user();
							if($user->isLoggedIn()){
						?>
						<a class="navbar-user"><?php echo escape($user->data()->username); ?></a>
						<?php }else{
							redirect::to('404.php');
						} ?>
						<a class="navbar-logout" href="logout.php" id="logout">Log out</a>

			 </div>
		 </div>
	</nav>

	<nav class="navbar col-lg-2" id="sidebar">
		<ul class="nav navbar-nav">
			 <li><a href="autori.php">Pretraga Autora</a></li>
			 <li><a href="dodajAutora.php">Dodaj Autora</a></li>
			 <li><a href="izmeniAutora.php">Izmeni Autora</a></li>
			 <li><a href="obrisiAutora.php">Obrisi Autora</a></li>
	 </ul>
	</nav>

  <div class="container container-login col-lg-10">
    <div class="jumbotron">
      <div class="container" id="autori">
				<?php
					$inputA = input::get('imeA');
					$autor = db::getInstance()->get('Autori', array(
						'firstName', '=', $inputA));
				?>
        <form class="form-group search-polje" action="" method="post">
          <span id="pretraga">Pretraga:</span>
          <input type="text" class="form-control col-lg-3" name="imeA" id="pretragaAutora" placeholder="Ukucajte ime Autora..." />

          <button type="submit" class="btn btn-primary" id="btnPretrazi">GO!</button>
        </form>
        <div class="container" id="tabela">
          <table class="table">

             <thead>
               <tr>
                 <th>ID</th>
                 <th>Ime </th>
                 <th>Prezime</th>
                 <th>Drustvo</th>
                 <th>Kontakt</th>
                 <th>Email</th>
                 <th>Adresa</th>
               </tr>
             </thead>

             <tbody>
							 <?php
							 foreach($autor->results() as $autor){
							 ?>
               <tr>
								 <td><?php echo escape($autor->autor_id);?></td>
								 <td><?php echo escape($autor->firstName);?></td>
								 <td><?php echo escape($autor->surname);?></td>
								 <td><?php echo escape($autor->society);?></td>
								 <td><?php echo escape($autor->phone);?></td>
								 <td><?php echo escape($autor->email);?></td>
								 <td><?php echo escape($autor->address);?></td>
               </tr>
							 <?php
								}
							?>
             </tbody>

          </table>
        </div>
      </div>
    </div>
  </div>


<!-- Latest compiled and minified JavaScript -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="js/login.js"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
