<?php
	require_once 'core/init.php';
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!--Bower css-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <title>Author App</title>
<!-- build:css public/css/style.css -->
	<link rel="stylesheet" href="css/acount.css">
<!-- endbuild -->
</head>

<body>
	<nav class="navbar navbar-custom">
		<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="glyphicon glyphicon-menu-hamburger"></span>
					</button>
					<a class="navbar-brand" href="profile.php">AuthorApp</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
						<li><a href="profile.php">Dela</a></li>
						<li><a href="search.php">Pretraga dela</a></li>
						<li><a href="autori.php">Autori</a></li>
						<li><a href="databse.php">Baza</a></li>
						<li><a href="account.php">Obracun</a></li>
						</ul>
						<?php
							$user = new user();
							if($user->isLoggedIn()){
						?>
						<a class="navbar-user"><?php echo escape($user->data()->username); ?></a>
						<?php }else{
							redirect::to('404.php');
						} ?>
						<a class="navbar-logout" href="logout.php" id="logout">Log out</a>

			 </div>
			 </div>
	</nav>

	<div class="container" id="obracun_table">
		<div class="jumbotron">
			<h3 id="h3_obracun">Obracun</h3>
			<span id="ime_prezime">Ime i Prezime Autora:</span>
			<input class="form-group" id="ime_autora" name="imeAutora" />

			<span id="period">Period Obracuna:</span>
			<span id="od">Od:</span>
			<input class="form-group" id="period_od" name="periodOd" placeholder="dd/mm/yyyy" />
			<span id="do">Do:</span>
			<input class="form-group" id="period_do" name="periodDo" placeholder="dd/mm/yyyy" /><br>

			<span id="poreskaStopa">Poreska stopa:</span>
			<input class="form-group" id="porez" name="porez" value="20%" disabled="true" />

			<span id="indexA">Index Autora:</span>
			<input class="form-group" id="index_autora" name="indexAutora" />

			<button class="btn btn-success" id="obracunaj" type="submit">Obracunaj</button>
		</div>

		<div class="container" id="obracunT">
			<table class="table" hidden>
				 <thead>
					 <tr>
						 <th>Ime </th>
						 <th>Prezime</th>
						 <th>Period Obracuna</th>
						 <th>Datum Obracuna</th>
						 <th>Index</th>
						 <th>Poresta stopa</th>
						 <th>Porez za nezaposlenost</th>
					 </tr>
				 </thead>
				 <tbody>
					 <tr>
						 <td></td>
						 <td> </td>
						 <td></td>
						 <td></td>
						 <td></td>
					 </tr>
				 </tbody>
			</table>
			<button class="btn btn-primary" type="submit" id="btn_stampaj">Stampaj</button>
		</div>
	</div>


<!-- Latest compiled and minified JavaScript -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
